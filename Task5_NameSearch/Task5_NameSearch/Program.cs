﻿using System;
using System.Collections.Generic;

namespace Task5_NameSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> nameList = new List<string>
            {
                "Jon Erik Selland",
                "Sondre Waage Tofte",
                "Anders Tranberg",
                "Bernt Odland",
                "Solveig Kopperdal"
            };
            nameList.Sort();
            List<string> searchNameList = new List<string>();

            do
            {
                searchNameList.Clear();
                Console.WriteLine("Type /quit to quit");
                Console.Write("Search for name: ");
                string inputName = Console.ReadLine();

                #region check input from user
                if (inputName == "")
                {
                    foreach (string name in nameList)
                    {
                        Console.WriteLine(name);
                    }
                }
                else if(inputName == "/quit")
                {
                    break;
                }
                else
                {
                    foreach (string name in nameList)
                    {
                        if (name.Contains(inputName, StringComparison.OrdinalIgnoreCase))
                        {
                            searchNameList.Add(name);
                        }
                    }
                }
                #endregion

                #region print the search results
                if (searchNameList.Count > 0)
                {
                    foreach (string name in searchNameList)
                    {
                        Console.WriteLine(name);
                    }
                }
                else
                {
                    if(inputName != "")
                    {
                        Console.WriteLine("No names in the list containing the search criteria");
                    }
                }
                #endregion

                Console.WriteLine();


            } while (true);
        }
    }
}
