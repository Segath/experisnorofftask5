﻿using System;
using System.Collections.Generic;

namespace Task10_UpgradedNameSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> listOfPersons = new List<Person>
            {
                new Person("Jon Erik", "Selland", "23452143", "Room"),
                new Person("Sondre", "Waage Tofte", "456243573", "Home"),
                new Person("Anders", "Tranberg", "+23457023452", "Denmark"),
                new Person("Bernt", "Odland", "234502340", "Bergen"),
                new Person("Solveig", "Kopperdal", "42092345", "Somewhere")
            };
            Search(listOfPersons);
        }

        public static void Search(List<Person> listOfPersons)
        {
            do
            {
                Console.WriteLine("Type /quit to quit");
                Console.Write("Search for name: ");
                string searchString = Console.ReadLine();

                #region check input from user and search
                if (searchString == "")
                {
                    foreach (Person person in listOfPersons)
                    {
                        Console.WriteLine(person.ToString());
                    }
                }
                else if (searchString == "/quit")
                {
                    break;
                }
                else
                {
                    foreach (Person person in listOfPersons)
                    {
                        if (person.ContainsName(searchString))
                        {
                            Console.WriteLine(person.ToString());
                        }
                    }
                }
                #endregion
                Console.WriteLine();
            } while (true);
        }
    }
}
